package net.evolin.lobby;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.wrapper.Wrapper;
import net.evolin.papercore.PaperCore;
import org.bukkit.plugin.java.JavaPlugin;

public final class Lobby extends JavaPlugin {

    private PaperCore paperCore;

    @Override
    public void onEnable() {
        // Plugin startup logic
        // get PaperCore Instance
        this.paperCore = PaperCore.getInstance();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        CloudNetDriver.getInstance().getEventManager().unregisterListeners(this.getClass().getClassLoader());
        Wrapper.getInstance().unregisterPacketListenersByClassLoader(this.getClass().getClassLoader());
    }

    /**
     * @return PaperCore - paperCore instance
     */
    public PaperCore getPaperCore() {
        return paperCore;
    }
}
