package net.evolin.common.handler;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;

import java.io.Serializable;
import java.util.Map;

public class LanguageHandler implements Serializable {

    public String defaultLanguage;
    // language, authority, key - value
    public Map<String, Map<String, Map<String, String>>> languages;

    /**
     * returns the key in the default language (normally english)
     *
     * @param key string - key is the internal name for the languageValue
     * @return returns null or a string
     */
    public String get(String key, String defaultValue) {
        return get(this.defaultLanguage, "unordered", key, defaultValue);
    }

    /**
     * returns the key in the wanted language if not exists it tries to find it in the default language (normally english)
     *
     * @param lang      string - english or german supported at the moment
     * @param authority string - authority where the key/value belongs to ex. global / lobby
     * @param key       string - key is the internal name for the languageValue
     * @return returns null or a string
     */
    public String get(String lang, String authority, String key, String defaultValue) {
        // if language does not exist -> return default value
        if (!languages.containsKey(lang)) {
            System.out.println("LanguageHandler: {%language%}/{%authority%}/{%key%} language not found".replace("%language%", lang).replace("%authority%", authority).replace("%key%", key));
            callback(lang, authority, key, defaultValue);
            return defaultValue;
        }

        // if authority does not exist -> return default value
        if (!languages.get(lang).containsKey(authority)) {
            System.out.println("LanguageHandler: {%language%}/{%authority%}/{%key%} authority not found".replace("%language%", lang).replace("%authority%", authority).replace("%key%", key));
            callback(lang, authority, key, defaultValue);
            return defaultValue;
        }

        //if language/authority does not has key -> return default value
        if (!languages.get(lang).get(authority).containsKey(key)) {
            System.out.println("LanguageHandler: {%language%}/{%authority%}/{%key%} key not found".replace("%language%", lang).replace("%authority%", authority).replace("%key%", key));
            callback(lang, authority, key, defaultValue);
            return defaultValue;
        }

        // return whatever is in the config
        return languages.get(lang).get(authority).get(key);
    }

    /**
     * function to callback to the module and add dependencies
     *
     * @param lang         string - ex. english, german
     * @param authority    string - ex. lobby, global
     * @param key          string - ex. spawnPoint
     * @param defaultValue string - ex. x:0,y:0,z:0
     */
    private void callback(String lang, String authority, String key, String defaultValue) {
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("missingLanguage", "", new JsonDocument()
                .append("language", lang)
                .append("authority", authority)
                .append("key", key)
                .append("value", defaultValue));
    }
}
