package net.evolin.common.objects;

import java.io.Serializable;
import java.util.UUID;

public abstract class PlayerObject<T> implements Serializable {

    private final String username;
    private final UUID uuid;
    private String language;

    /**
     * creates a player object with username and uuid (example for bungee)
     *
     * @param username username from the player
     * @param uuid     uuid from the player
     */
    public PlayerObject(String username, UUID uuid) {
        this.language = "english";
        this.username = username;
        this.uuid = uuid;
    }

    /**
     * creates a player object with username and uuid (example for bungee)
     *
     * @param language language for the user
     * @param username username from the player
     * @param uuid     uuid from the player
     */
    public PlayerObject(String language, String username, UUID uuid) {
        this.language = language;
        this.username = username;
        this.uuid = uuid;
    }

    /**
     * @return returns players default language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language sets the players default language
     */
    protected void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return String returns players string
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return UUID returns players uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * sends a message to the player
     *
     * @param message String - Message to the player
     */
    protected abstract void sendMessageToPlayer(String message);

    /**
     * @return returns the Player (could be Bukkit Player, Bungee ProxiedPlayer, CloudNet ICloudPlayer)
     */
    public abstract T getPlayer();

}
