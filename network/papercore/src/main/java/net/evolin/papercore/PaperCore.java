package net.evolin.papercore;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceId;
import de.dytanic.cloudnet.wrapper.Wrapper;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.papercore.listener.ConfigListener;
import net.evolin.papercore.listener.PlayerListener;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

public final class PaperCore extends JavaPlugin {

    private ServiceId serviceId = Wrapper.getInstance().getServiceId();
    private LanguageHandler languageHandler = null;
    private ConfigHandler configHandler = null;
    private static PaperCore paperCore;
    private HashMap<UUID, PlayerObject> uuidPlayerObjectHashMap = new HashMap<>();


    @Override
    public void onEnable() {
        // Plugin startup logic
        paperCore = this;

        // registers all normal PlayerListeners (tab and so on)
        Listener playerListener = new PlayerListener(this);
        this.getServer().getPluginManager().registerEvents(playerListener, this);
        CloudNetDriver.getInstance().getEventManager().registerListener(playerListener);

        // register for CloudNet config Event
        CloudNetDriver.getInstance().getEventManager().registerListener(new ConfigListener(this));

        // calls a sendChannelMessage to request the current configuration after start
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("requestConfig", String.valueOf(this.serviceId.getUniqueId()), new JsonDocument());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        CloudNetDriver.getInstance().getEventManager().unregisterListeners(this.getClass().getClassLoader());
        Wrapper.getInstance().unregisterPacketListenersByClassLoader(this.getClass().getClassLoader());
    }

    public LanguageHandler getLanguageHandler() {
        return languageHandler;
    }

    public void setLanguageHandler(LanguageHandler languageHandler) {
        this.languageHandler = languageHandler;
    }

    public ConfigHandler getConfigHandler() {
        return configHandler;
    }

    public void setConfigHandler(ConfigHandler configHandler) {
        this.configHandler = configHandler;
    }

    public PlayerObject getUuidPlayerObject(UUID uuid) {
        return uuidPlayerObjectHashMap.get(uuid);
    }

    public void addUuidPlayerObject(UUID uuid, PlayerObject playerObject) {
        this.uuidPlayerObjectHashMap.put(uuid, playerObject);
    }

    public void removeUuidPlayerObject(UUID uuid) {
        this.uuidPlayerObjectHashMap.remove(uuid);
    }

    public ServiceId getServiceId() {
        return serviceId;
    }

    /**
     * Returns a instance that the plugin can be used as "api" in other Plugins
     *
     * @return returns the paper instance
     */
    public static PaperCore getInstance() {
        return paperCore;
    }
}
