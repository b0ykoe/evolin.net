package net.evolin.papercore.objects;

import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class PlayerObject extends net.evolin.common.objects.PlayerObject<Player> {

    private Player player;

    public PlayerObject(Player player) {
        super(player.getName(), player.getUniqueId());
        this.player = player;
    }

    public PlayerObject(String username, UUID uuid) {
        super(username, uuid);
        this.player = Bukkit.getPlayer(uuid);
    }

    public PlayerObject(String language, String username, UUID uuid) {
        super(language, username, uuid);
        this.player = Bukkit.getPlayer(uuid);
    }

    @Override
    protected void sendMessageToPlayer(String message) {
        this.player.sendMessage(new TextComponent(message));
    }

    @Override
    public Player getPlayer() {
        return this.player;
    }
}
