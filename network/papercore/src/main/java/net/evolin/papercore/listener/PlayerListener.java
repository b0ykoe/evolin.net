package net.evolin.papercore.listener;

import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerListener implements Listener {

    private final JavaPlugin plugin;
    private final PaperCore paperCore;

    /**
     * @param paperCore paperCore instance
     */
    public PlayerListener(PaperCore paperCore) {
        this.plugin = paperCore;
        this.paperCore = paperCore;
    }

    /**
     * adds a playerObject to the HashMap
     *
     * @param event PlayerJoinEvent
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void handle(PlayerJoinEvent event) {
        PlayerObject playerObject = new PlayerObject(event.getPlayer());
        this.paperCore.addUuidPlayerObject(event.getPlayer().getUniqueId(), playerObject);
    }

    /**
     * removes the playerObject from the HashMap
     *
     * @param event PlayerQuitEvent
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(PlayerQuitEvent event) {
        this.paperCore.removeUuidPlayerObject(event.getPlayer().getUniqueId());
    }
}
