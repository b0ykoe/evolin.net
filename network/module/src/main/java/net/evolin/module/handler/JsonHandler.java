package net.evolin.module.handler;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.module.NodeCloudNetModule;

import java.io.File;

public class JsonHandler {

    private JsonDocument config;
    private String relativePath;
    private NodeCloudNetModule module;

    /**
     * Constructor for a config file
     * @param module send module for folder naming
     * @param relativePath name for the config file
     */
    public JsonHandler(NodeCloudNetModule module, String relativePath) {
        this.module = module;
        this.relativePath = relativePath;
        this.config = this.loadConfig();
    }

    /**
     * check if this.config is set, if not load the config
     * @return returns the config
     */
    public final JsonDocument getConfig() {
        if (this.config == null) {
            this.config = this.loadConfig();
        }

        return this.config;
    }

    /**
     * overrides the current config
     * @param config JsonDocument of a config
     */
    public void setConfig(JsonDocument config) {
        this.config = config;
    }

    /**
     * reloads (overrides) the configuration
     * @return
     */
    public final JsonDocument reloadConfig() {
        return this.config = this.loadConfig();
    }

    /**
     * safes the current configuration file as file
     * @return returns a JsonHandler
     */
    public final JsonHandler saveConfig() {
        if (this.config != null) {
            this.config.write(new File(this.module.getModuleWrapper().getDataFolder(), this.relativePath));
        }
        return this;
    }

    /**
     * check if the file exists
     * @return returns true if the config file already exists
     */
    public boolean exists() {
        return new File(this.module.getModuleWrapper().getDataFolder(), this.relativePath).exists();
    }

    /**
     * loads a config file, creates the folder and file if not exists
     * @return returns the config
     */
    private JsonDocument loadConfig() {
        this.module.getModuleWrapper().getDataFolder().mkdirs();
        File file = new File(this.module.getModuleWrapper().getDataFolder(), this.relativePath);
        if (!file.exists()) {
            (new JsonDocument()).write(file);
        }
        return JsonDocument.newDocument(file);
    }

}
