package net.evolin.module.handler;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import net.evolin.module.Module;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FileHandler implements Runnable{

    public HashMap<String, HashMap<String, String>> config = new HashMap<>();
    public HashMap<String, HashMap<String, HashMap<String, String>>> messages = new HashMap<>();
    private boolean configWork = false;
    private boolean messageWork = false;
    private Module module;
    private Thread t;
    private boolean exit = false;
    public FileHandler(Module module) {
        this.module = module;
    }

    public void run() {
        try {
            while(!exit) {
                if(!config.isEmpty()) {
                    for(Iterator<Map.Entry<String, HashMap<String, String>>> it = config.entrySet().iterator(); it.hasNext(); ) {
                        Map.Entry<String, HashMap<String, String>> entry = it.next();
                        entry.getValue().forEach((key, value) -> this.module.config.getConfig()
                                .getDocument("config", new JsonDocument())
                                .getDocument(entry.getKey(), new JsonDocument())
                                .getString(key, value));
                        it.remove();
                    }
                    configWork = true;
                } else if (configWork) {
                    this.module.config.saveConfig();
                    this.module.sendConfig();
                    configWork = false;
                }

                if(!messages.isEmpty()) {
                    for(Iterator<Map.Entry<String, HashMap<String, HashMap<String, String>>>> it = messages.entrySet().iterator(); it.hasNext(); ) {
                        Map.Entry<String, HashMap<String, HashMap<String, String>>> entry = it.next();
                        // language
                        entry.getValue().forEach((key, value) -> {
                            //authority
                            value.forEach((key2, value2) ->  {
                                //key, value
                                this.module.messages.getConfig()
                                        .getDocument("languages", new JsonDocument())
                                        .getDocument(entry.getKey(), new JsonDocument())
                                        .getDocument(key, new JsonDocument())
                                        .getString(key2, value2);
                            });
                        });
                        it.remove();
                    }
                    messageWork = true;
                } else if (messageWork) {
                    this.module.messages.saveConfig();
                    this.module.sendConfig();
                    messageWork = false;
                }

                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            System.out.println("FileHandler Thread interrupted.");
        }
    }

    public void start () {
        if (t == null) {
            t = new Thread (this);
            t.start ();
        }
    }

    public void stop () {
        if(t != null) {
            this.exit = true;
            t = null;
        }
    }
}
