package net.evolin.module;

import com.google.gson.Gson;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.module.ModuleLifeCycle;
import de.dytanic.cloudnet.driver.module.ModuleTask;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.module.NodeCloudNetModule;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.module.commands.ModuleCommand;
import net.evolin.module.handler.FileHandler;
import net.evolin.module.handler.JsonHandler;
import net.evolin.module.listener.ConfigListener;

public final class Module extends NodeCloudNetModule {

    public JsonHandler messages;
    public JsonHandler config;
    public LanguageHandler languageHandler;
    public ConfigHandler configHandler;
    public FileHandler fileHandler = new FileHandler(this);


    /**
     * order 64 -> first start
     * Loads the configurations
     */
    @ModuleTask(order = 64, event = ModuleLifeCycle.STARTED)
    public void init() {
        loadConfig();
        fileHandler.start();
    }

    /**
     * stops everything
     */
    @ModuleTask(order = 64, event = ModuleLifeCycle.STOPPED)
    public void stop() {
        this.fileHandler.stop();
    }

    /**
     * order 32 -> seconds start
     * registers all listeners
     */
    @ModuleTask(order = 32, event = ModuleLifeCycle.STARTED)
    public void registerListeners() {
        CloudNetDriver.getInstance().getEventManager().registerListener(new ConfigListener(this));
    }

    /**
     * order 16 -> third start
     * registers all commands (depending on configurations)
     */
    @ModuleTask(order = 16, event = ModuleLifeCycle.STARTED)
    public void registerCommands() {
        registerCommand(new ModuleCommand(this));
    }

    /**
     * Sends the configurations to a specific server
     * @param serviceInfoSnapshot ServiceInfoSnapShot from the Server
     */
    public void sendConfig(ServiceInfoSnapshot serviceInfoSnapshot) {
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage(serviceInfoSnapshot, "receiveConfig", "messages", this.messages.getConfig());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage(serviceInfoSnapshot, "receiveConfig", "config", this.config.getConfig());
    }

    /**
     * Sends the configurations to a specific server, ALL will be accepted by every server
     */
    public void sendConfig() {
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("receiveConfig", "messages", this.messages.getConfig());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("receiveConfig", "config", this.config.getConfig());
    }

    /**
     * reloads the configurations and renews the handlers.
     */
    public void loadConfig() {
        this.messages = new JsonHandler(this, "messages.json");
        this.languageHandler = new Gson().fromJson(this.messages.getConfig().toJson(), LanguageHandler.class);
        this.config = new JsonHandler(this, "config.json");
        this.configHandler = new Gson().fromJson(this.config.getConfig().toJson(), ConfigHandler.class);
    }
}