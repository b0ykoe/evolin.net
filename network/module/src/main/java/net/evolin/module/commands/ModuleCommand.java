package net.evolin.module.commands;

import de.dytanic.cloudnet.command.Command;
import de.dytanic.cloudnet.command.ICommandSender;
import de.dytanic.cloudnet.common.Properties;
import net.evolin.module.Module;

public class ModuleCommand extends Command {

    private Module module;

    /**
     * Command for CloudNet
     *
     * @param module passing trough our module to access public objects
     */
    public ModuleCommand(Module module) {
        super("evolin", "evo");

        this.module = module;
        this.permission = "evolin.command.module";
        this.prefix = "EvolinNet";
        this.description = this.module.languageHandler.get(this.module.languageHandler.defaultLanguage, "module", "module-description", "Module for the Evolin plugin pack.");
    }

    @Override
    public void execute(ICommandSender sender, String command, String[] args, String commandLine, Properties properties) {
        // check if there are arguments and if null send help
        if (args.length == 0) {
            sendHelp(sender);
            return;
        }

        // if there are arguments check for the first one
        // check if the first agument is reload
        if (args[0].equalsIgnoreCase("reload")) {
            // if is reload then load (overrides current config) and sends to all services
            this.module.loadConfig();
            this.module.sendConfig();

            // send a message to the sender to confirm that the config was successfully reload
            sender.sendMessage(this.module.languageHandler.get(this.module.languageHandler.defaultLanguage, "module", "module-command-reload-successfully", "The languagefile was reloaded and send to all servers."));
        }
    }

    /**
     * Sends a full list of the Evolin-Module commands
     *
     * @param sender ICommandSender - sender from the cloudNet console or ingame sender (mostly some sort of ICloudPlayer)
     */
    private void sendHelp(ICommandSender sender) {
        sender.sendMessage("evolin reload");
    }
}
